class Tf < Formula
  desc "Terraform 0.12 wrapper"
  homepage "https://gitlab.com/slava-kuzmenko/go-tf.git"
  url "https://d3ijgzy8tupcqd.cloudfront.net/0.12.10/tf_0.12.10_darwin_amd64.zip"
  version "0.12.10"
  sha256 "2d305e54c7c0f0b1135046bc421371dea05f3ea938fed826785900cb67008b8a"

  def install
    bin.install "tf"
    output = Utils.popen_read("#{bin}/tf completion bash")
    (bash_completion/"tf").write output
    output = Utils.popen_read("#{bin}/tf completion zsh")
    (zsh_completion/"_tf").write output
  end
end